
# Coding Tips

* Asking for clarifications from the interviewer
* Think out loud
* Explain time complexity and memory complexity
* Write expected input and output as test cases
* Edge cases, edge cases, edge cases

