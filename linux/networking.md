
# Linux Kernel Network Stack

## Important Data structures

* struct sk_buff
    - where a package is stored
    - `include/linux/skbuff.h`
    - used by multiple layers, MAC L3, IP L3, TCP/UDP L4
    - Maintained as a doubely linked list
* struct net_device
    - represent a network device
    - `include/linux/netdevice.h`

## Exposed interface to userspace

In `/proc/net` and in `/proc/sys/net/`

## Netlink

Netlink is the preferred interface between user space and kernel for IP networking conf.

You can open a Netlink socket like `socket(PT_NETLINK, SOCK_DGRAM)`

With Netlink sockets, endpoints are usually identified by the PID, and a 0 means the kernel. You can send uni/multicast to PID/GID and/or processes and register to get notified on certain netlink events.

## Notification chains

