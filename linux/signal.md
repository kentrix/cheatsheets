
                                                                  ┌──────────┬────────────────┬────────────────────────────────────────────────────┐
                                                                  │ Signal   │ Default Action │                    Description                     │
                                                                  ├──────────┼────────────────┼────────────────────────────────────────────────────┤
                                                                  │SIGABRT   │       A        │ Process abort signal.                              │
                                                                  │SIGALRM   │       T        │ Alarm clock.                                       │
                                                                  │SIGBUS    │       A        │ Access to an undefined portion of a memory object. │
                                                                  │SIGCHLD   │       I        │ Child process terminated, stopped,                 │
                                                                  │          │                │ or continued.                                      │
                                                                  │SIGCONT   │       C        │ Continue executing, if stopped.                    │
                                                                  │SIGFPE    │       A        │ Erroneous arithmetic operation.                    │
                                                                  │SIGHUP    │       T        │ Hangup.                                            │
                                                                  │SIGILL    │       A        │ Illegal instruction.                               │
                                                                  │SIGINT    │       T        │ Terminal interrupt signal.                         │
                                                                  │SIGKILL   │       T        │ Kill (cannot be caught or ignored).                │
                                                                  │SIGPIPE   │       T        │ Write on a pipe with no one to read it.            │
                                                                  │SIGQUIT   │       A        │ Terminal quit signal.                              │
                                                                  │SIGSEGV   │       A        │ Invalid memory reference.                          │
                                                                  │SIGSTOP   │       S        │ Stop executing (cannot be caught or ignored).      │
                                                                  │SIGTERM   │       T        │ Termination signal.                                │
                                                                  │SIGTSTP   │       S        │ Terminal stop signal.                              │
                                                                  │SIGTTIN   │       S        │ Background process attempting read.                │
                                                                  │SIGTTOU   │       S        │ Background process attempting write.               │
                                                                  │SIGUSR1   │       T        │ User-defined signal 1.                             │
                                                                  │SIGUSR2   │       T        │ User-defined signal 2.                             │
                                                                  │SIGPOLL   │       T        │ Pollable event.                                    │
                                                                  │SIGPROF   │       T        │ Profiling timer expired.                           │
                                                                  │SIGSYS    │       A        │ Bad system call.                                   │
                                                                  │SIGTRAP   │       A        │ Trace/breakpoint trap.                             │
                                                                  │SIGURG    │       I        │ High bandwidth data is available at a socket.      │
                                                                  │SIGVTALRM │       T        │ Virtual timer expired.                             │
                                                                  │SIGXCPU   │       A        │ CPU time limit exceeded.                           │
                                                                  │SIGXFSZ   │       A        │ File size limit exceeded.                          │
                                                                  │          │                │                                                    │
                                                                  └──────────┴────────────────┴────────────────────────────────────────────────────┘
       The default actions are as follows:

       T     Abnormal termination of the process.

       A     Abnormal termination of the process with additional actions.

       I     Ignore the signal.

       S     Stop the process.
