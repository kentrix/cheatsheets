## Process states

`sched.h`
```
/* Used in tsk->state: */
#define TASK_RUNNING			0x0000
#define TASK_INTERRUPTIBLE		0x0001
#define TASK_UNINTERRUPTIBLE		0x0002
#define __TASK_STOPPED			0x0004
#define __TASK_TRACED			0x0008
/* Used in tsk->exit_state: */
#define EXIT_DEAD			0x0010
#define EXIT_ZOMBIE			0x0020
#define EXIT_TRACE			(EXIT_ZOMBIE | EXIT_DEAD)
/* Used in tsk->state again: */
#define TASK_PARKED			0x0040
#define TASK_DEAD			0x0080
#define TASK_WAKEKILL			0x0100
#define TASK_WAKING			0x0200
#define TASK_NOLOAD			0x0400
#define TASK_NEW			0x0800
#define TASK_STATE_MAX			0x1000
```

## Killing zombie

Zombie process can't be kill by `kill -9`, instead, send a `SIGCHLD` to the `$PPID`.

Zombie processes are also knows as `defunc` processes in `ps`

## Processes and interrupt context

Kernel code can be entered in two ways, there is no kernel thread/process executing these, the process themselves execute them.

* Process context
    - The kernel is entered from a syscall or processor exception (eg. page fault), Synchronous
* Interrupt context 
    - The kernel is entered from a hardware interrupt, kernel data worked upon. Asyncronous
    - IRQ -> ISR

Kernel code can use `preempt.h:in_task()` marco to determine the context. and `current` marco for current `struct task_struct` 

### Interrupt context

When executing an interrupt handler, the kernel is in interrupt context. They cannot block.

### Enable/Disable Interrupts

local_irq_disable(); on the processor.

local_irq_save/restore() to save irq state/mask

disable_irq(_nosync) to mask a single IRQ line. To all processors

### Bottom halves

* Original bottom half - simple interface but globally sync, bottleneck - Gone
* Task queues - Gone
    - A linked list of functions to call, performance is not too good in critical systems like networking
* Softirq and Tasklets
    - Softirq are a set of statically defined BHs that can run parallel on any proc
    - Tasklets are flexible, dynamic BH build on top of softirqs
* Work Queues

#### SoftIRQs

Static, at compile time. Softirq does not preempt another softirq, the only preempt is an ISR. 
Exe'ed by `ksoftirqd` kernel thread/return from interrupt, or networking subsystem code

Used by networking and block devices access.

The softirq handlers run with interrupts enabled and cannot sleep.While a handler
runs, softirqs on the current processor are disabled.Another processor, however, can exe-
cute other softirqs. If the same softirq is raised again while it is executing, another proces-
sor can run it simultaneously.This means that any shared data—even global data used only
within the softirq handler—needs proper locking


#### Tasklets

Very similar to SoftIRQ.

When tasklet gets sched on procs, it will disable local interrupts, run it, and reanble interrupts.

To you easily write a Tasklet and schedule them with kernel api.

#### ksoftirqd

Kernel thread gets ran on each processor to keep schedule softirqs

#### work queues

Runs in the process context, if the work needs to sleep, then work queue should be used.

Managed and sched by `kworker` kernel thread


## Kernel threads
* Only executed in Kernel space
* Pid 0 is the swapper process, calls init/kthreadadd and exit
* Pid 1 is init
* Pid 2 is the parent kernel thread, it spawns the rest of the kernel threads

Two main types:
* Thread started and waits until requested by the kernel to perform an action
* Perioidic kernel tasks

Examples:
* kswap, perioidcally sync mem pages from cache to disk
* managed deferred actions
* transcation for FS journals

## Process kerel datastructure
`include/linux/sched.h` (a scheduling entity)

`struct task_struct {}` contains info like:
* used cpu for CFS
* Priority/Schedule info
* state
* stack ptr
* pid, tgid
* mm (memory mgmt struct)
    - Segment
    - page table
    - usage info
* exit codes
* parent/children
* capability
* FDs/VFS data
* cgroups
* locks
* timers
* AIO context
* signals
* rlimit
* SElinux context
* Children tasks
* many more

We require one stack per thread per privilege level support by the CPU. Thus, every user space thread alive has two stacks:
* User space stack
* Kernel space stack
    - small, fixed size, ~2 pages typically

But for kernel threads, there are just their kernel space stack.

The stack can be checked via `/proc/<pid>/stack` or with `eBPF` [BCC/bpftrace/bpfcc]

## Process and threads
* A process contains (conceptually) at least 1 thread of execution
* In Linux, a `thread` is just a process with Shared VAS with another process, except the *stack*
    - Typically created with `clone()` with `CLONE_SHAREVM`

Threads can be shown by `ps -LA`, where LWP (light weight process) is the id of the thread, (TID==LWP), if LWP != PID, then it is a thread. Again, every process must have at 1 thread. Thread are typically created via `pthreads` library.

A thread a mostly a `task_struct` with a different `task_struct->stack` ptr.

## tgid

In the kernel, each thread has its own ID, called a PID, although it would possibly make more sense to call this a TID, or thread ID, and they also have a TGID (thread group ID) which is the PID of the first thread that was created when the process was created.

When a new process is created, it appears as a thread where both the PID and TGID are the same (currently unused) number.

When a thread starts another thread, that new thread gets its own PID (so the scheduler can schedule it independently) but it inherits the TGID from the original thread.

i.e new thread has new LWP/TID and parent's TGID

## Process creation
Via syscalls
`clone` `fork` and `exec` `execve`

* `vfork()` is similar to fork but page table entries of the parent is not copied, instead, the child exe as the sole thread in the parent's address space, and the parent is blocked until the child either calls `exec` or exists.

* `clone()` is typically used to create threads with `CLONE_VM` flag, a normal fork is just `clone(SIGCHLD, 0)`

## COW

Linux's fork implements copy-on-write pages, rather than deping the process address space on fork, the parent/child can share one, and only when a page in child is written, the kernel makes a copy.

## execve

* calls `do_execve`
    - opens exe file
    - `bprm_init`
        - `mm_alloc`
        - `init_new_context`
    - `prepare_binprm`
    - copy env
    - `search_binary_handler` # Search for the handler, ELF, shabangs

Here, if the SUID/SGID is set, the EUID/EGIT will be set to whatever is on the `inode`.

It:
* Release all resourced used by the old proc
* Maps the binary into the VAS
* Setup the stack pointer
* Setup env and cli args

## Binary formats supported by Linux

* flat_format, embedded systems to save space
* script_format, shabangs
* misc_format, wine/java byecode
* elf_format
* elf_fdpic_format 
* irix_format
* som_format
* aout_format 


