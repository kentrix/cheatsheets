
# LKM

Linux Kernel Module framework provides support for compiling kernel code outside kernel source tree.

`.ko` files 

## User space cmds
* `insmod`
* `modprobe`
* `rmmod`
* `depmod` - create a list of module deps

## Modules
* On arch-Linux
    - `ls /lib/modules/5.13.13-arch1-1/` 

`modprobe.d` (man 5), config dir for modprod



