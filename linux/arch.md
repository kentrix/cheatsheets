# Kernel Architecture

## Kernel Space Components
* Core kernel
    - Process/thread creation/desturction, scheduling, signals, timers, interrupt, namespaces, cgroups, LKM support, crypto etc
* Memory Management
    - Virtual Address Space (VAS)
    - Memory allocation and mapping
* VFS
    - Virtual File System
* Block IO
    - Between VFS to actual device
* Network stack
    - to-RFC impls
* IPC
    - Message Queue
    - Shared memory
    - Semaphores
* Sound support
* KVM supporm

* Arch-specific code
* Kernel init
* SELinux
* Device drivers


