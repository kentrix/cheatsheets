# Virtual memory

https://www.kernel.org/doc/html/latest/admin-guide/mm/index.html

Linux memory management subsystem is responsible, as the name implies, for managing the memory in the system. This includes implementation of virtual memory and demand paging, memory allocation both for kernel internal structures and user space programs, mapping of files into processes address space and many other cool things.

Linux memory management is a complex system with many configurable settings. Most of these settings are available via /proc filesystem and can be quired and adjusted using sysctl. These APIs are described in Documentation for /proc/sys/vm/ and in man 5 proc.


## VAS layout for a process
* Text segment
    - code, r-x
* Initalized data segment
    - init'd vars, rw-
* BSS
    - uninited vars, zeroed, rw-
* Heap
    - malloc'd via `brk()` or `mmap()`
* Libraries
    - `.so` shared libs mapped into VAS, r-x/rw-
* Stack
    - CPU dependent but mostly grows down

## VAS/VM split

With a page size of 4KiB (2^12), for 
* 32bit -> 20bit for addressing
* 64bit -> 48bit for addressing
    - or 5 level page table, 56bit addressing 

For x86 32bit, VM split is by default 2:2, `0x800000000 (PAGE_OFFSET)`, but it is configurable. 

For x84_64, VM split is 128TB bottom:128TB top `0xffff 8000 0000 0000`.

* UVA, User Virtual Address
* KVA, Kernel Virtual Address

### Kernel segments/VAS

Kernel Logical Address:

* 32bit
    - Low kernel segment is directly mapped to PHY, this is called *kernel logical address*. This is physically contiguous.
    - ALL physcially mem is mapped here (is possible)
    - x86 typically 768MB, eg, 2GiB mem system will only have part of it mapped
    - Rest is mapped to `ZONE_HIGHMEM`
* 64bit
    - The whole kernel segment is directly mapped to PHY

Kernel Segment
|-------------------------|----------------------------------------|
| Direct map              |vmalloc/ioremap, modules/ DMA, reserved |
| Low mem                 |arch vectory page/vDSO, etc region      |
| Kernel Logical Address  |                                        |
--------------------------------------------------------------------  

Kernel vmalloc region:
* Virtual
* Allocated via `vmalloc()`, used in kernel codes
* aka `ioremap` space

Kernel module space:
* shared with above
* For LKMs

The kernel itself is mapped into the KVS, marcos like `_text, __init_begin, _sdata, __bss_start`. Can be seen via `/proc/iomem`

## Single-page kernel stacks

Since 2.6, Linux kernel offers 4KB size for kernel stacks, this means interrupts handlers, which used to share kernel stacks, won't fit anymore. The solution is to use a new concept of "interrupt stack".

### Trap page
The null trap page is a very small page on the low mem with no perm, accessing it will trigger a page fault then a `SIGSEGV`.

### KASAN

Kernel Address SANitizer is a compiler time instrumentation tool to detect memory issues such like UAF or OOB, since Kernel 4.x. This is also an region in the Kernel VAS.


## VAS for linux

* 32bit, VAS -> 4GiB
* 64bit, VAS -> 16EiB

Each process has their **OWN** USER VAS, BUT they share the SAME kernel segment!

## Exploring process VAS
In `procfs` -> `/proc/<pid>/maps`

```
cat /proc/self/maps
start_uva      end_uva  perm/mode start-off mj:mn inode#                   image-name
564abaff0000-564abaff2000 r--p 00000000 fe:01 731584                     /usr/bin/cat
564abaff2000-564abaff7000 r-xp 00002000 fe:01 731584                     /usr/bin/cat
564abaff7000-564abaffa000 r--p 00007000 fe:01 731584                     /usr/bin/cat
564abaffa000-564abaffb000 r--p 00009000 fe:01 731584                     /usr/bin/cat
564abaffb000-564abaffc000 rw-p 0000a000 fe:01 731584                     /usr/bin/cat
564abc580000-564abc5a1000 rw-p 00000000 00:00 0                          [heap]
7f18373be000-7f18379a7000 r--p 00000000 fe:01 783199                     /usr/lib/locale/locale-archive
7f18379a7000-7f18379a9000 rw-p 00000000 00:00 0
7f18379a9000-7f18379cf000 r--p 00000000 fe:01 722043                     /usr/lib/libc-2.33.so
7f18379cf000-7f1837b1a000 r-xp 00026000 fe:01 722043                     /usr/lib/libc-2.33.so
7f1837b1a000-7f1837b66000 r--p 00171000 fe:01 722043                     /usr/lib/libc-2.33.so
7f1837b66000-7f1837b69000 r--p 001bc000 fe:01 722043                     /usr/lib/libc-2.33.so
7f1837b69000-7f1837b6c000 rw-p 001bf000 fe:01 722043                     /usr/lib/libc-2.33.so
7f1837b6c000-7f1837b77000 rw-p 00000000 00:00 0
7f1837b7a000-7f1837b9c000 rw-p 00000000 00:00 0
7f1837b9c000-7f1837b9d000 r--p 00000000 fe:01 722026                     /usr/lib/ld-2.33.so
7f1837b9d000-7f1837bc1000 r-xp 00001000 fe:01 722026                     /usr/lib/ld-2.33.so
7f1837bc1000-7f1837bca000 r--p 00025000 fe:01 722026                     /usr/lib/ld-2.33.so
7f1837bca000-7f1837bcc000 r--p 0002d000 fe:01 722026                     /usr/lib/ld-2.33.so
7f1837bcc000-7f1837bce000 rw-p 0002f000 fe:01 722026                     /usr/lib/ld-2.33.so
7fff84205000-7fff84227000 rw-p 00000000 00:00 0                          [stack]
7fff84323000-7fff84327000 r--p 00000000 00:00 0                          [vvar]
7fff84327000-7fff84329000 r-xp 00000000 00:00 0                          [vdso]
ffffffffff600000-ffffffffff601000 --xp 00000000 00:00 0                  [vsyscall] # syscall helper, optimisation
```

Frontend for this: `pmap` `smem`


### VMA
Virtual Memory Area.

It is a kernel datastructure `current->mm->mmap`, this contains all info for the kernel to perform various form of memory management, eg: **servicing page faults*, *caching file content for I/O in/out of the kernel page cache*.

`include/linux/mm_types.h`


## Page tables and VAS->PHY translation
* 32bit -> 2 Level page table
    - Page Global Dir
    - Page Table
    - Offset
* 64bit -> 4 Level page table, 5 level page table support in kernel too
    - PGD
    - PUD
    - PMD
    - PTE
    - offset

Hierarchically linked page tables are used to support the rapid and efficient management of large address
spaces.

`PMD_SHIFT` specifies the total number of bits used by a page., 2^PMD_SHIFT bytes per page

Arch specific, eg: `include/asm-x86/pgtable_64.h`

The final table has specific entries (PTE):
* PAGE_PRESENT whether the page in mem
* PAGE_ACCESSED count
* PAGE_DIRTY
* PAGE_FILE whether the page is NOT in memory
* PAGE_USER whether userspace is allowed to access
* PAGE_READ/WRITE/EXECUTE 

Pagetable is a arch/cpu concept, the hardware expect the pointer to the page table in their model specified register.



## [K]ASLR
(Kernel) Address Space Layout Randomization

A way to prevent OOB attacks in C.

The virtual memory mapping is random each time it is mapped via `mmap` etc.

Since kernel 2.6.12 for User ASLR, kernel 3.14 for KASLR. 

Tunable via `/proc/sys/kernel/randomize_va_space` or via kernel params, default is 2

## NUMA

Linux treat UMA as a special case of NUMA with a single node.

Each PHY mem bank will have their own ZONEs laied out:
* Zone DMA
* Zone DMA32
* Zone NORMAL

in `/proc/buddyinfo`

`struct pglist_data` is the base element used to represent a node, it holds
* node_zones
* node_zonelists
* nr_zones
* node_mem_map
* node_start_pfn
* node_id
 


## Memory allocation

User space allocations:
glibc: `malloc`

For small allocations, `malloc` uses `sbrk` to increase the heap (program break)
For larger allocations, `malloc` uses `mmap` for allocation

Syscalls:
`brk` `mmap`

The do_mmap()function is used by the kernel to create a new linear address interval.

### Page cache & write back

 the Linux kernel implements a disk cache called the page cache.The goal of this cache is to minimize
disk I/O by storing data in physical memory that would otherwise require
disk access.

File writes are cached by the kernel and periodically written back into the disk, as write back mechanism.

Backing with a Radix treevia `address_space` object + offset value


### Buddy system Allocator, BSA

The entire Linux kernel users the BSA for (de)allocation, including LKM and device drivers.

BSA allocate memory from the Kernel Logical Address. A user space `malloc` call **indirectly** results in calls
to the BSA/SLUB.

Low memory/Kernel Logcal Addr is **NOT-SWAPPABLE**.

The BSA is build on top of *freelists*, 2^0, 2^1... 2^10 pages chunks. Each "order" is a circular linked list. The `MAX_ORDER` varies by arch.

The kernel keeps *multiple BSA freelists*, one for each NUMA node:zone.

To allocate memory, the next size up is allocated, then split off into smaller chunks.

To deallocate, the chunk is looked up in the buddylist then restored.

Wastage is a problem in BSA, result in a saw-tooth waste diagram.

DS is in `include/linux/mmzone.h`

Advatage of BSA:
* Helps defragment memory (prevent external fragmentation)
* Guarantees physically contiguous memory pages
* Guarantee CPU cache line-aligned blocks
* O(log n)

API:
* `alloc_page()`
* `alloc_pages()`
* `free_page()`
* `free_pages()`

### GFP flags
Get Free Page flags is enum:
* GFP_KERNEL
* GFP_ATOMIC
* Any kernel internal ones, omitted

If in process context and it is safe to sleep, use the GFP_KERNEL ﬂag. If it
is unsafe to sleep (typically, when in any type of atomic or interrupt
context), you must use the GFP_ATOMIC ﬂag. Invoking any function that might block in when is it unsafe to sleep, such as interrupt or atomic context, IS A **BUG**. There is a kernel config to debug this. `CONFIG_DEBUG_ATOMIC_SLEEP`


### Slab/SLUB allocator

Old impl called `SLAB` via `CONFIG_SLAB`, newer kernel uses `CONFIG_SLUB`

A layer on top of the BSA.

Typicalls, slabs are made of single page, and each cache can consist of multiple slabs. Then each slabs contain some number of objects. 

Cache -> Slab -> Objects

see `struct slab`

Each slab can be in one of the thress states:
* Full
* Partial
* Empty



* Used to cache kernel objects, those frequency used, `slabtop` to inspec, also `/proc/slabinfo`
* Mitigate wastage
* Physcially contiguous
* Hardware cache aligned
* You can write your own easily, `kmem_cache_create()`
* Very fast, perf gains
* *Also* caches some BSA pages
* Has a size limit by the `MAX_ORDER` -> 4MiB for x86_64 with `MAX_ORDER` of 11 -> 2^10 pages * 4KiB = 4MiB, will fail if size > max size

Notable ones:
* pid
* buffer_head
* dentry
* inode_cache
* kmalloc-*
* task_struct

```
 Active / Total Objects (% used)    : 4874509 / 7806894 (62.4%)
 Active / Total Slabs (% used)      : 199289 / 199289 (100.0%)
 Active / Total Caches (% used)     : 125 / 167 (74.9%)
 Active / Total Size (% used)       : 1031599.63K / 1495439.20K (69.0%)
 Minimum / Average / Maximum Object : 0.01K / 0.19K / 9.62K

  OBJS ACTIVE  USE OBJ SIZE  SLABS OBJ/SLAB CACHE SIZE NAME
3584490 2747663  76%    0.10K  91910       39    367640K buffer_head
1304320 399792  30%    0.03K  10190      128     40760K lsm_inode_cache
695268 182115  26%    0.19K  33108       21    132432K dentry
455193 316666  69%    1.16K  16859       27    539488K ext4_inode_cache
286244 232041  81%    0.57K  10223       28    163568K radix_tree_node
271552 162999  60%    0.06K   4243       64     16972K vmap_area
129216  53127  41%    0.06K   2019       64      8076K kmalloc-rcl-64
110376  43303  39%    0.19K   5256       21     21024K kmalloc-192
107920  94251  87%    0.20K   5396       20     21584K vm_area_struct
105344 101240  96%    0.50K   3292       32     52672K kmalloc-512
102102  21382  20%    0.04K   1001      102      4004K ext4_extent_status
 86336  34483  39%    0.25K   2698       32     21584K kmalloc-256
 59584  49760  83%    0.06K    931       64      3724K anon_vma_chain
 51584  50623  98%    0.12K   1612       32      6448K kernfs_node_cache
 41208  41208 100%    0.08K    808       51      3232K task_delay_info
 40066  30875  77%    0.09K    871       46      3484K anon_vma
 35072  30307  86%    0.03K    274      128      1096K kmalloc-32
 34230  13011  38%    0.09K    815       42      3260K kmalloc-rcl-96

 ```


APIs:
* `kmalloc()`
* `kzalloc()`
* `kfree()`
* `devm_kmalloc()`, resource managed, no need to free

It will return Kernel Logical Address (still virtual but offsetted), and physcially contiguous.

### Slab shrinkers
`kswapd*` reclaim memory as part of their chores, slab cache code is expected to impl a shrinker interface.


## Cgroups and Memory

Cgroups can manages via the `memcg` files in `/sys/cgroup`, namely `memory.min` and `memory.low` for protection and `memory.high`/`memory.low` for mem usage.

## OOM killer

Last line of defense against thrashing.

`kswapd` kernel thread continously monitor mem usage and invoke page reclaim during memory pressure, uses *watermark levels*, in `/proc/zoneinfo` (min, low, high). But when we really ran out, we need OOM to help us. The watermark level is managed via `/proc/sys/vm/min_free_kbytes`, the reserve grows as a proportion of the main memory.

OOM killer can be invoked via *Magic SysRq*, in `/proc/sys/kernel/sysrq`.

OOM killer is normally invoked when `__do_fault() ->... __alloc_pages_nodemask()` fails, i.e when a page fault handler fails.

OOM errors can be dumped by setting `/proc/sys/vm/oom_dump_tasks` to `1`

`/proc/<pid>/oom_score`
    * Value 0 to 1000
    * It is a value saying how much memory is actually being used, 0 -> 0%, 1000 -> 100%
    * The higher the score, means they get killed first

`/proc/<pid>/oom_score_adj`
    * value 1000 -> -1000
    * oom_score + oom_score_adj = net_oom_score


### Overcommit

* `/proc/sys/vm/overcommit_memory`
    - 0: allow overcommiting via heruistic, default
    - 1: always overcommit, userful for sparse memory apps
    - 2: don't overcommit, useful for RT/reliable apps
* `/proc/sys/vm/overcommit_ratio`
    - Percentage of PHY allowed to be overcommitted


## vmalloc region and API

The kernel has BSA/SLUB allocator, but for high mem, vritual pages can also be allocated at will, from the `vmalloc/ioremap` region.

Allocates memory from `VMALLOC_START` -> `VMAALOC_END-1`

Min 1 Page, duh.

APIs:
`vmalloc()`
`vzalloc()`
`vfree()`
`__vmalloc()`, allow you to specify protection and GFP

Allocates virtually congtiugous memory, guarentee to be page-aligned.

API should be only called from a **process context** as it might cause the caller to sleep, useful when requesting large memory where `kmalloc` can't provide.

Internally, this uses demand paging, so new page will only be physically allocated when page fault arises.

## DMA and CMA
DMA (Direct Memory access)

CMA (Contiguous Memory Allocator), for DMA on memory-hungry devices, code is in DMA layer.


