# Linux System calls

A system call is just a userspace request of a kernel service.  In other words, a system call is just a C kernel space function that user space programs call to handle some request.

Linux kernel provides a set of these function for each arch. Typicall a `syscall` instruction jumps to a register target address. 

The kernel is responsible for providing custom functions for handling syscalls as well as writing the address of the function into the register for `syscall` lookups on startup. The kernel defines a range of macros to define syscall handlers, eg `SYSCALL_DEFINE3`.

It is not possible for user-space applications to execute kernel code directly.They cannot
simply make a function call to a method existing in kernel-space because the kernel exists
in a protected memory space. If applications could directly read and write to the kernel’s
address space, system security and stability would be nonexistent.

Instead, user-space applications must somehow signal to the kernel that they want to
execute a system call and have the system switch to kernel mode, where the system call
can be executed in kernel-space by the kernel on behalf of the application.

On x86, the interrupt number is 0x80

## vsyscall and vdso

x64 has 547 syscalls, vsyscall and vdso are designed to speed up syscalls.

On x64, the last 8MB in VAS is for vsyscall `sudo cat /proc/1/maps | grep vsyscall`, this means syscalls will be execuated in the userspace and there will not be context switching

vDSO replaced the vsyscall in recent kernels, it maps memory pages into each process in a shared object form, it is `linux-vdso.so.1`, each ELF should have it -> `ldd /bin/uname` and `sudo cat /proc/1/maps | grep vdso` 

## syscall

The syscall of them all, aka indirect system call. `long syscall(long number, ...)`

## read

Read a file from a FD, set ERRNO when failure

## write

Write buf to target FD

## open
Open a file and get a FD back

```c
       int open(const char *pathname, int flags);
       int open(const char *pathname, int flags, mode_t mode);
       int creat(const char *pathname, mode_t mode);
       int openat(int dirfd, const char *pathname, int flags);
       int openat(int dirfd, const char *pathname, int flags, mode_t mode);
       int openat2(int dirfd, const char *pathname,
                   const struct open_how *how, size_t size);
```

## close

Close a FD

## lseek

`off_t lseek(int fd, off_t offset, int whence);`

repositions the file offset of the open file description associated with the file descriptor fd to the argument offset according to the directive whence (a flag)

## fcntl

manipulate file descriptor, `int fcntl(int fd, int cmd, ... /* arg */ );`

Can:
* Duplicate FD
* Set/Get FD
* Set/Get Status flags (access mode) 
* Advisory record locking
* Mandatory locking
* Managing signals
* dnotify

## stat, fstat, lstat, fstatat

       stat() and fstatat() retrieve information about the file pointed to by pathname; the differences for fstatat() are described below.

       lstat() is identical to stat(), except that if pathname is a symbolic link, then it returns information about the link itself, not the file that the link refers to.

       fstat() is identical to stat(), except that the file about which information is to be retrieved is specified by the file descriptor fd.

## poll, ppoll

 wait for some event on a file descriptor

## mmap, munmap

map or unmap files or devices into memory

## sigaction, rt_sigaction

examine and change a signal action

The sigaction() system call is used to change the action taken by a process on receipt of a specific signal. 

## sigprocmask, rt_sigprocmask

sigprocmask()  is  used to fetch and/or change the signal mask of the calling thread.  The signal mask is the set of signals whose delivery is currently blocked for the caller

## sigreturn, rt_sigreturn 
return from signal handler and cleanup stack frame

## pread/pwrite

Read/Write to a FD at given offset

## access, faccessat, faccessat2

check user's permissions for a file

## pipe, pipe2

Create pipe

## select

synchronous I/O multiplexing, allows a program to monitor multiple file descriptors, waiting until one or more of the file descriptors become "ready" for some class of I/O operation.

## sched_yield

yield the processor

## mremap

remap a virtual memory address

## msync

synchronize a file with a memory map

## mincore

determine whether pages are resident in memory

## madvise

The  madvise() system call is used to give advice or directions to the kernel about the address range beginning at address addr and with size length bytes In most cases, the goal of such advice is to
       improve system or application performance.

## shmget, shmat, shmctl

shared mem mgmt

## dup, dup2, dup3
 
duplicate a file descriptor

## pause

wait for signal

## nanosleep

high-resolution sleep

## socket

Create a socket for comm `int socket(int domain, int type, int protocol);`

## connect

initiate a connection on a socket `int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);`

## accept, accept4 

accept a connection on a socket

## bind 

bind a name to a socket, When  a  socket is created with socket(2), it exists in a name space (address family) but has no address assigned to it.  bind() assigns the address specified by addr to the socket referred to by the
       file descriptor sockfd.  addrlen specifies the size, in bytes, of the address structure pointed to by addr.  Traditionally, this operation is called “assigning a name to a socket”.

## listen

listen for connections on a socket, `int listen(int sockfd, int backlog);`

## ioctl

The ioctl system call is used to issue commands to the device (via its driver).

Can be used as a debug interface or generic IO.

Too big :(

## fork

Full blown fork, copies everything. Still has COW to reduce overhead.

## vfork

typically `vfork` then `execve` to load a new program. Lite fork

## clone

Start a thread, flags to determine what to copy.
