
# Booting

* PSU start up
* Upon getting `power good signal`, motherboard tries to start the CPU
* Start x86 CPU in real mode, with 20 bit address bus. Limited memory addressing, helped with segments.
* BIOS/EFI finds the bootable medium
* Runs arch specified code to setup initial memory/stack/bss etc
* Transition to protected mode and runs code to detect mem/cpu and setup heap
* Transition into long mode/64bit 
* Kernel decompresses itself, the initalramfs (gzipped)
* Initramfs allows the kernel to understand stuff like extFS, crypt so that it can go to next step
* Uncompress the kernel image in memory and chroot into it
* Start init