
# Kernel notion of time

Typically a hardware clock which has a tick rate, and it generates a hardware interrupt at fix intervals.

100HZ on i386 systems.

Now a configurable parameter.

Higher HZ -> Higher timer resolution.

## Jiffies

Global `jiffies` olds the number of ticks that have occurred since the system boot.

### RTC

Real-time-clock, in CMOS

### System timer

A hardware component, programmable.

## Timers 

Timers—sometimes called dynamic timers or kernel timers—are essential for managing the
flow of time in kernel code. Kernel code often needs to delay execution of some function
until a later time.

Timers handlers are exed by SoftIRQs.

Another way of delay exe can be done via `jiffies` and busy waiting.

Or `schedule_timeout` will put the thread to sleep for at least X amount of time.