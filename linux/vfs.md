
# Linux VFS

is the subsystem of the kernel that implements the file and filesystem-related
interfaces provided to user-space programs.All filesystems rely on the VFS to enable them
not only to coexist, but also to interoperate.

It enable syscalls like open, read, write to work regardless of the filesystem.

## Unix FS

A filesystem is a hierarchical storage of data adhering to a specific structure. Filesystems
contain files, directories, and associated control information



## VFS objects

The four primary object types of the VFS are

* The superblock object, which represents a specific mounted filesystem.
* The inode object, which represents a specific file.
* The dentry object, which represents a directory entry, which is a single component of a path.
* The file object, which represents an open file as associated with a process.

## Superblock

The superblock is a data structure containing information
about the filesystem as a whole. Sometimes the collective data is referred to as filesystem
metadata. Filesystem metadata includes information about both the individual files and the
filesystem as a whole.

The superblock object is implemented by each filesystem and is used to store information
describing that specific filesystem.This object usually corresponds to the filesystem
superblock or the filesystem control block, which is stored in a special sector on disk (hence
the object’s name). Filesystems that are not disk-based (a virtual memory–based filesys-
tem, such as sysfs, for example) generate the superblock on-the-fly and store it in memory.

The most important item in the superblock object is s_op, which is a pointer to the
superblock operations table.

## Dentry

the VFS employs the concept of a directory entry (dentry).A dentry is
a specific component in a path. Using the previous example, /, bin, and vi are all dentry
objects.

The first two are directories and the last is a regular file.This is an important
point: Dentry objects are all components in a path, including files.

dentry object is not physically stored on the disk, no flag in struct
dentry specifies whether the object is modified (that is, whether it is dirty and needs to
be written back to disk).

A valid dentry object can be in one of three states: used, unused, or negative. A used dentry corresponds to a valid inode (d_inode points to an associated inode)
and indicates that there are one or more users of the object (d_count is positive).A used
dentry is in use by the VFS and points to valid data and, thus, cannot be discarded.

An unused dentry corresponds to a valid inode (d_inode points to an inode), but the
VFS is not currently using the dentry object (d_count is zero). Because the dentry object
still points to a valid object, the dentry is kept around—cached

A negative dentry is not associated with a valid inode (d_inode is NULL) because either
the inode was deleted or the path name was never correct to begin with.The dentry is
kept around, however, so that future lookups are resolved quickly.\

Note that struct dentry is associated with a inode for quick look up. --> the purpose

### Dcache

After the VFS layer goes through the trouble of resolving each element in a path name
into a dentry object and arriving at the end of the path, it would be quite wasteful to
throw away all that work. Instead, the kernel caches dentry objects in the dentry cache or,
simply, the dcache.

The dentry cache consists of three parts:

* Lists of “used” dentries linked off their associated inode via the i_dentry field of
the inode object. Because a given inode can have multiple links, there might be
multiple dentry objects; consequently, a list is used.

* A doubly linked “least recently used” list of unused and negative dentry objects.

* A hash table and hashing function used to quickly resolve a given path into the
associated dentry object.

## File object

User space typically works with file objects.

Each process maintain `struct file_struct` for open files, FDs are contained in it along with use count etc.

# Block IO

When a block is stored in memory—say, after a read or pending a write—it is stored in a
buffer. Each buffer is associated with exactly one block.The buffer serves as the object that
represents a disk block in memory.

## I/O Scheduler

Simply sending out requests to the block devices in the order that the kernel issues them,
as soon as it issues them, results in poor performance. This is esp true for spinning disks.

The job of I/O scheduler is to cache file I/O and arrange in a fashion to optimise pref.

* Elevator
* Deadline I/O Scheduler , prevents starvation
* Anticipatory I/O - Min read latency at expense of global throughput
* CFQ - Completely Fair Queuing
* Noop 





## Special VFS

### sysfs

Since 2.6

The sysfs tree encompasses the following (among other things):
* Every bus present on the system (it can be a virtual or pseudo bus as well)
* Every device present on every bus
* Every device driver bound to a device on a bus

### debugfs

For kernel debugging purposes.

```
debugfs on /sys/kernel/debug type debugfs (rw,nosuid,nodev,noexec,relatime)
```

### procfs

Purpose:
* Provider interface for getting internal info for proc/hardware
* Tweaking /proc/sys or `sysctl`

* `/proc/PID` info about the process
```
arch_status  cgroup      coredump_filter     environ  gid_map  loginuid   mountinfo   ns         oom_score_adj  root       setgroups     stat     task            uid_map
attr         clear_refs  cpu_resctrl_groups  exe      io       map_files  mounts      numa_maps  pagemap        sched      smaps         statm    timens_offsets  wchan
autogroup    cmdline     cpuset              fd       latency  maps       mountstats  oom_adj    personality    schedstat  smaps_rollup  status   timers
auxv         comm        cwd                 fdinfo   limits   mem        net         oom_score  projid_map     sessionid  stack         syscall  timerslack_ns
```

### procfs stuff

* buddyinfo
    - buddy allocator info
* cgroups
    - cgroups hierarchy
* cmdline
    - linux kernel arg
* cpuinfo
* crypto
    - kernel crypto drivers
* devices
    - list of ALL char/block devs
* diskstats
* fb
    - framebuffer
* filesystems
   - list of supported fs
* interrupts
* iomem
    - io mapped memory
* ioports
    - major/minor devs for io
* kallsyms
    - ALL kernel symbols
* keys
    - Kernel crypto keys
* kmsg
    - kernel messages
* kpagecgroup
* loadavg
* locks
    - kernel locks
* meminfo
* modules
    - kernel module and their memory addr
* mounts
* mtd
    - bios info
* mtrr
 -   Memory Type Range Registers (MTRRs) some system only
* pagetypeinfo
    - page type list, similar to buddysystem in order of magnitude
* partitions
    - block dev partitions
* schedstat
* slabinfo
* softirqs
    - softirqs stat on each cpu
* stat
* swaps
* sysrq-trigger 
* thread-self
* timer_list
    - list of all timers
* uptime
* version
* vmallocinfo
    - Full list of alloaced VM area
* zoneinfo
    - mem zone info for each cpu 

## per proc stuff

* io
    - iostat
* sched
    - scheduler stats
* wchan
    - wait chan
* fd
    - dir of open fds
* stack
* task
    - dir of sub tasks
* oom_score
* oom_score_adj
* oom_adj
* status
* smaps
    - VAS map
* ns
    - namespace info



