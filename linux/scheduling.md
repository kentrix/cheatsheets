
# Linux scheduling



Linux has modular scheduling classes, since 2.6.23

## Scheduling classes 

Scheduling classes are used to decided which task to run next. A scheduling policy is mapped to one of the scheudling classes.

```
// kernel/sched/sched.h
[ ... ]
extern const struct sched_class stop_sched_class;
extern const struct sched_class dl_sched_class;
extern const struct sched_class rt_sched_class;
extern const struct sched_class fair_sched_class;
extern const struct sched_class idle_sched_class;
```

## KSE
Kernel Scheduling Entity

In Linux, a KSE is a thread, but a process contains at least 1 thread. This includes Kernel Threads

`struct sched_entity`, contains many statistics assocaited with a thread.

## Scheduling policy

* SCHED_NORMAL
* SCHED_RR
* SCHED_FIFO
* SCHED_BATCH
* SCHED_IDLE

Each `task_struct->policy` points to one of the policy

`struct sched_class` holds the modular scheduling class the thread belong to.

Policy AND Scheduling class are dynamic and can be set easily, via `chrt`

* SCHED_FIFO/SCHED_RR maps to rt_sched_class
* SCHED_OTHER maps to fair_sched_class
* swapper/0 maps to idle_sched_class`

picking invoked via `schedule() --> __schedule() --> pick_next_task()`, the actually iteration calls the class's code. It will go down the list of sched classes, if one member is empty, it will go to the next one.

Eg. rt_sched_class is empty, kernel will go to fair_sched_class and pick a task there.

The run queue is maintained for each NUMA core.

In normal desktop system, only fair_sched_class and idle_sched_class are non-empty.

## Run Queue
The central data structure of the core scheduler that is used to manage active processes is known as the
run queue. Each CPU has its own run queue, and each active process appears on just one run queue.

## Wait Queue

Sleeping is handled via wait queues. A wait queue is a simple list of processes waiting for
an event to occur.

Waking is handled via wake_up(), which wakes up all the tasks waiting on the given wait
queue. It calls try_to_wake_up(), which sets the task’s state to TASK_RUNNING, calls
enqueue_task() to add the task to the red-black tree, and sets need_resched if the
awakened task’s priority is higher than the priority of the current task.

## CFS

Completely Fair Scheduler.

* Uses rbtree and a linked list internally
* Keeps track the `vruntime`, the time the task spend running on CPU. Update each tick is called
* Param can be tweaked via `/proc/sys/kernel/sched_cfs_bandwidth_slice_us`

During scheduling `pick_next_task`, the task that has spent the least amount of time, which is the leftmost leaf node on the tree will be picked, with O(1), (Note: RB tree balancing is O(n))

## Scheduler code runs

Scheduling is carried out by the process contexts themselves, the regular threads that run on the CPU!

Within the timer interrupt (Soft IRQ) (in the code of`kernel/sched/core.c:scheduler_tick()`,
wherein interrupts are disabled), the kernel performs the meta work
necessary to keep scheduling running smoothly; this involves the constant
updating of the per CPU runqueues as appropriate, load balancing work, and
so on. No scheduling code is called here tho. This will set `TIF_NEED_RESCHED`, if the `vruntime` is higher than another thread (change the RB tree).

There is a global flag called `need_resched` or `TIF_NEED_RESCHED` mask in threadinfo, this will trigger a call to `schedule()` on the next opportunity.

There are few **scheduling opportunity points** in the kernel, eg:
* Return from syscall
* Return from interrupt

## Kernel Preemption

Kernel since 2.6 is Preemptive, is it safe to reschedule as long as the `current` task does not hold a lock. This is done via checking the `preempt_count` in the `thread_info`, when is it 0, it is safe to resched.


## Context switch

Arch dependent, but commonly
* MM switch, update MM related registers
* CPU switch, update program counter register
* Save the old ones into memory
* calls `switch_to`

## CPU affinity 

Which CPU to run on, a bit mask, managd via `taskset`. For SMP processors.

The kernel does rebalacing to managed the load across CPUs. This is done by the kernel thread `migration_thread`

## Priorities

This is true for (soft)RT KSEs or the O(1) scheduler, this does not apply to the CFS.

It is not sufficient to consider just the static priority. But three of them:

* Dynamic priority `task_struct->prio`
* Normal priority `task_struct->normal_prio`
* Static priority `task_struct->static_prio`

## Niceness

From -20 to +19, +19 being the niceest :)

This is a poportion of the CPU time relative to other tasks 

## Real-time threads

For SCHED_FIFO/SCHED_RR, a value from 1 to 99, 99 being highest

## Interrupts

Interrupts enable hardware to signal to the processor, hardwares generate them and signals the CPU to run the interrupt handler.

IRQ 0 is the timer interrupt

ISR are just simple C functions and kernel invokes them in response to interrupts and they run in the interrupt context. 

ISR has to be short and result to CPU execution ASAP, so traditionally, ISR is split into top halves and bottom halves.



