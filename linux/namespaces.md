
# Linux namespaces

* Mount
* Process
* IPC
* Network
* User
* UTC (hostname/domain)
* Cgroup
* time

## NS details

### process

Parent has view into pid in subnamespace, but the pid in subnetspace has no knowledge of the parent.

### Networking

Each process see a different set of networking interfaces

Child NS will need bridge network.

### Mount

Mounting virtual disks


## Utils

`lsns` to list namespaces, init spawns the global NS for all ns
`ushare` to change namespace
`nsenter`