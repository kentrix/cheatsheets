
# Synchronization in Linux

In mordern SMP system, process synchronization is important for the kernel to function well.

With Preemption, the process can be interrupt at any moment, datarace can happen if care is not taken.

## Kernel Syncronization

### Critical section

A code path that is prone to race, must be protected from concurrent access. A critical section is a piece of code that must run either exclusively; that is, alone (serialized), or atomically; that is, indivisibly, to completion, without
interruption.

## Locks


Let's say that `t_ctxsw`
represents the time to context switch. As we've learned,
the minimal cost of the mutex lock/unlock operation is
`2 * t_ctxsw`.
Now, let's
say that the following expression is true:
`t_locked < 2 * t_ctxsw` i.e the time spend within critical section is less than two context switches, will lead to thrashing.


### Spin locks

* Low overhead
* Used when the context cannot sleep (atomic/interrupt context)
* IRQ-disabling variant, has medium overhead
* IRQ-saving (save the interrupt mask), high overhead

```
spinlock_t lock;
spin_lock_init(&lock);
void spin_lock(spinlock_t *lock);
void spin_unlock(spinlock_t *lock);
```

#### RW lock
* Exclusive write, multi-reader lock

```
#include <linux/rwlock.h>
rwlock_t mylist_lock;
```

### Semaphores and Mutex

* Higher overhead
* Used sleep is needed and safe

```
void __sched mutex_lock(struct mutex *lock);
void __sched mutex_try_lock(struct mutex *lock); // returns immediately instead of blocking
void __sched mutex_unlock(struct mutex *lock);
void __sched mutex_lock_interruptible(struct mutex *lock);
```

## atomic types

### atomic_t

* Older
* `linux/atomic.h`

### refcount_t

* `linux/refcount.h`

## RMW
Read-Modify-Write

```
Non-RMW ops:
atomic_read(), atomic_set()
atomic_read_acquire(), atomic_set_release()
RMW atomic operations:
Arithmetic:
atomic_{add,sub,inc,dec}()
atomic_{add,sub,inc,dec}_return{,_relaxed,_acquire,_release}()
atomic_fetch_{add,sub,inc,dec}{,_relaxed,_acquire,_release}()
Bitwise:
atomic_{and,or,xor,andnot}()
atomic_fetch_{and,or,xor,andnot}{,_relaxed,_acquire,_release}()
Swap:
atomic_xchg{,_relaxed,_acquire,_release}()
atomic_cmpxchg{,_relaxed,_acquire,_release}()
atomic_try_cmpxchg{,_relaxed,_acquire,_release}()
Reference count (but please see refcount_t):
atomic_add_unless(), atomic_inc_not_zero()
atomic_sub_and_test(), atomic_dec_and_test()
Misc:
atomic_inc_and_test(), atomic_add_negative()
atomic_dec_unless_positive(), atomic_inc_unless_negative()
```

## Per CPU Vars

Some vars are special and are associated with CPU nodes, these vars does not need locking.
